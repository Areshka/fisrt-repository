<?php
include_once("inc/lib.inc.php");
set_error_handler("myError");
include_once("inc/data.inc.php");

/* * �������� ������� ��� � ���� ������ �� 00 �� 23 * �
�������� ������ � ������ ����� �� 0 �� 23 */
$hour = (int) strftime('%H');
$welcome = ''; //�������������� ���������� ��� �����������
if ( $hour > 0 and $hour < 6 ) {
    $welcome = "������ ����";
}elseif ( $hour >= 6 and $hour < 12) {
    $welcome = "������ ����";
}elseif ( $hour >= 12 and $hour < 18 ) {
    $welcome = "������ ����";
}elseif ( $hour >=18 and $hour < 23) {
    $welcome = "������ �����";
}else{
    $welcome = "������ ����";
}
//��������� ������ � ����� �������� ����
setlocale(LC_ALL, "ru_RU.windows-1251");
$day = strftime('%d');
$mon = strftime('%B');
$year = strftime('%Y');

//������������� ����������
$title = "���� ����� �����";
$header = "$welcome, �����!";
$id = strtolower(strip_tags(trim($_GET['id'])));
switch($id){
    case 'about':
        $title = '� �����';
        $header = '� ����� �����';
        break;
    case 'contact':
        $title = '��������';
        $header = '�������� �����';
        break;
    case 'table':
        $title = '������� ���������';
        $header = '������� ���������';
        break;
    case 'calc':
        $title = '������ �����������';
        $header = '�����������';
        break;
}
?>
<!DOCTYPE html>
<html>

<head>
    <title><?= $title; ?></title>
    <link rel="stylesheet" href="style.css" />
</head>

<body>

<div id="header">
    <!-- ������� ����� �������� -->
    <?php
    include_once("inc/top.inc.php");
    ?>
    <!-- ������� ����� �������� -->
</div>

<div id="content">
    <!-- ��������� -->
    <h1> <?= $header; ?></h1>
    <!-- ��������� -->
    <!-- ������� ��������� �������� -->

    <?php

    switch($id){
        case 'about':
            require_once("about.php");
            break;
        case 'contact':
            require_once("contact.php");
            break;
        case 'table':
            require_once("table.php");
            break;
        case 'calc':
            require_once("calc.php");
            break;
        default:
            require_once("inc/index.inc.php");
            break;
    }
    ?>
    <!-- ������� ��������� �������� -->
</div>
<div id="nav">
    <!-- ��������� -->
    <?php
    include_once("inc/menu.inc.php");
    ?>
    <!-- ��������� -->
</div>
<div id="footer">
    <!-- ������ ����� �������� -->
    <?php
    include_once("inc/bottom.inc.php");
    ?>
    <!-- ������ ����� �������� -->
</div>
</body>

</html>