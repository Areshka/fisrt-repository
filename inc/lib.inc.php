<?php
//������� ��������� ������
function myError ($errno, $errmsg, $errfile, $errline) {
    $dt = date("d-m-Y H:i:s");
    $str = $dt . " - " . $errmsg . " in " . $errfile . ":" . $errline . "." . PHP_EOL;
    switch ($errno) {
        case E_USER_ERROR:
        case E_USER_WARNING:
        case E_USER_NOTICE:
            echo $errmsg; // ���������� � ������ �������� ������������
            error_log($str, 3, "error.log"); //����� ��������� ���������� �� ������ ���������� ���� � ����
    }
}

//������� ��������� ��������������� � ������������� ����
function drawMenu ($menu, $vertical = TRUE) {
    if(!is_array($menu))
        return FALSE;
    $style= "";
    if ($vertical == FALSE) {
        $style = 'style = "display:inline; margin-right:10px"';
    }
    echo "<ul>";
    foreach($menu as $key => $value_menu){
        echo "<li $style>";
        echo "<a href=\"" . $value_menu['href'] . "\">" . $value_menu['link'] . "</a>";
        echo "</li>";
    }
    echo "</ul>";
    return TRUE;
}

//������� ��������� ������� ���������
function drawTable ($rows, $cols, $color) {
    echo "<table border='1'>";
    for ($tr=1; $tr<=$rows; $tr++) {
        echo "<tr>";
        for ($td=1; $td<=$cols; $td++) {
            if ($tr == 1 or $td == 1) {
                $style = "style = 'background-color:$color'";
                echo "<th $style>" . $tr * $td . "</th>";
            }else{
                echo "<td>" . $tr * $td . "</td>";
            }
        }
        echo "</tr>";
    }
    echo "</table>";
}
?>